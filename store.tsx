import { configureStore } from '@reduxjs/toolkit'
import counterReducer from "./slices/counterSlices";
import jobReducer from "./slices/jobsSlice";

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    jobs: jobReducer
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch