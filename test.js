const express = require('express');
const { Storage } = require('@google-cloud/storage');
const multer = require('multer');

const app = express();
const port = process.env.PORT || 3000;

// Initialize Google Cloud Storage client
const storage = new Storage({
    keyFilename: './G_Cloud_Key.json', // Replace with your service account key file
    projectId: 'primary-projects-396007', // Replace with your Google Cloud Project ID
});

// Create a bucket object
const bucket = storage.bucket('primary_storage'); // Replace with your bucket name

// Configure multer for file uploads
const multerStorage = multer.memoryStorage();
const upload = multer({ storage: multerStorage });

app.post('/upload', upload.single('file'), (req, res) => {
    const file = req.file;
    if (!file) {
        return res.status(400).send('No file uploaded.');
    }

    // Define the destination path in the bucket
    const destination = 'uploads/' + file.originalname;

    // Upload the file to Google Cloud Storage
    const blob = bucket.file(destination);
    const blobStream = blob.createWriteStream();

    blobStream.on('error', (err) => {
        return res.status(500).send('Error uploading file: ' + err.message);
    });

    blobStream.on('finish', () => {
        const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
        return res.status(200).send(`File uploaded successfully. View it <a href="${publicUrl}" target="_blank">here</a>.`);
    });

    blobStream.end(file.buffer);
});


app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
