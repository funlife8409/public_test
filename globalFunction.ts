import { Welcome } from "./type";
type apiErrorMessage = {
    status: "error",
    message: String;
    data: any
}
type apiSuccessMessage = {
    status: "success",
    message?: String;
    data: any
}
export const createApiErrorMessage = ({status, message, data}:apiErrorMessage) => {
    return {
        status: status || "error",
        message: message,
        data: data || null
    }
}
export const createApiSuccessMessage  = ({status, message, data}:apiSuccessMessage) => {
    return {
        status: status || "success",
        data: data || null
    }
}