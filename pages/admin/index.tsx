
//@ Components
import setting from "../../components/layout/setting.json";
import Header from '../../components/layout/Header';

import Head from 'next/head'
import Script from 'next/script' // Import Script from next/script
// import Image from 'next/image'
import { Inter } from '@next/font/google'
// import styles from '@/styles/Home.module.css'
import { GetStaticProps, NextPage } from 'next'
import { Welcome } from '../../type'

//@ Components
import LCS from '../../components/admin/NJ_Left_CS'; // Left Content Section
import RCS from '../../components/admin/NJ_Right_CS'; // Left Content Section

import { useDispatch } from 'react-redux'
import { addMultiJob } from '../../slices/jobsSlice'
import { useEffect } from 'react'

const inter = Inter({ subsets: ['latin'] })

interface props {
    status?: "success",
    data: Welcome[]
}

export const getStaticProps: GetStaticProps<props> = async () => {
    try {
        const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/job`);
        if (!res.ok) {
            throw new Error(`Failed to fetch, status: ${res.status}`);
        }
        const { data }: props = await res.json();
        return {
            props: { data },
            revalidate: 600, // Revalidate every 10 minutes
        };
    } catch (error) {
        console.error('Error fetching data:', error);
        return {
            props: { data: [] }, // Handle error case
        };
    }
};


const Home: NextPage<props> = ({ data }) => {
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(addMultiJob(data))

    }, [dispatch, data])

    return (
        <>
            <Head>
                <title>Create New Job</title>
                <meta name="description" content="Find Government jobs" />
                <meta http-equiv="Cache-Control" content="max-age=600" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
                {/* <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
                <script src='/plugin/multiselect/multiselect.js'></script>
                <script src='/plugin/ckeditor/ckeditor.js'></script>
                <script src='/custom.js'></script> */}
            </Head>
            <Script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js" strategy="afterInteractive" />
            <Script src='/plugin/multiselect/multiselect.js' strategy="afterInteractive" />
            <Script src='/plugin/ckeditor/ckeditor.js' strategy="afterInteractive" />
            <Script src='/custom.js' strategy="lazyOnload" />
            <main>
                <Header menu={setting.adminHeader} />
                <div className="container-fluid">
                    <div className="row my-5">
                        {/* <div className="row"> */}
                        <div className="col-8">
                            <div className='container'>
                                <LCS />
                            </div>
                        </div>
                        <div className="col-4">
                            <RCS />
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}
export default Home;