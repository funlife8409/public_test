import { GetStaticProps, NextPage } from 'next'
import Image from 'next/image';
import setting from "../../../components/layout/setting.json";
import Header from '../../../components/layout/Header';

const Home: NextPage = () => {
    return (
        <>
            <Header menu={setting.adminHeader} />
            <table className="table align-middle mb-0 bg-white">
                <thead className="bg-light">
                    <tr>
                        <th>
                            <h4 className="bg-light m-0 p-3">Jobs List</h4>
                        </th>
                        <th colSpan={4}>
                            <input
                                type="text"
                                placeholder="Search"
                                className="form-control"
                                style={{ padding: '8px', border: '1px solid', float: 'right' }}
                            />
                        </th>
                    </tr>
                    <tr>
                        <th>Name</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Position</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div className="d-flex align-items-center">
                                <Image
                                    src="https://mdbootstrap.com/img/new/avatars/8.jpg"
                                    alt=""
                                    width={45}
                                    height={45}
                                    className="rounded-circle"
                                />
                                <div className="ms-3">
                                    <p className="fw-bold mb-1">
                                        UPPSC Civil Judge PCS J Pre Recruitment 2022 Status / Re Upload Photo and Signature for 303 Post
                                    </p>
                                    <span className="text-muted mb-0" title="Author">Author: Shreya Sinha</span>
                                    <span className="text-muted mb-0"> | Updated On: </span>
                                    <span className="text-muted mb-0" title="Updated on">20/08/2024</span>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p className="fw-normal mb-1">Last Date: 20/08/2024</p>
                            <p className="text-muted mb-0">Total Post: 1035</p>
                        </td>
                        <td>
                            <span className="badge badge-success rounded-pill d-inline">Active</span>
                        </td>
                        <td>Senior</td>
                        <td>
                            <button type="button" className="btn btn-link btn-sm btn-rounded">
                                Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className="d-flex align-items-center">
                                <Image
                                    src="https://mdbootstrap.com/img/new/avatars/6.jpg"
                                    alt=""
                                    width={45}
                                    height={45}
                                    className="rounded-circle"
                                />
                                <div className="ms-3">
                                    <p className="fw-bold mb-1">Alex Ray</p>
                                    <p className="text-muted mb-0">alex.ray@gmail.com</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p className="fw-normal mb-1">Consultant</p>
                            <p className="text-muted mb-0">Finance</p>
                        </td>
                        <td>
                            <span className="badge badge-primary rounded-pill d-inline">Onboarding</span>
                        </td>
                        <td>Junior</td>
                        <td>
                            <button
                                type="button"
                                className="btn btn-link btn-rounded btn-sm fw-bold"
                                data-mdb-ripple-color="dark"
                            >
                                Edit
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div className="d-flex align-items-center">
                                <Image
                                    src="https://mdbootstrap.com/img/new/avatars/7.jpg"
                                    alt=""
                                    width={45}
                                    height={45}
                                    className="rounded-circle"
                                />
                                <div className="ms-3">
                                    <p className="fw-bold mb-1">Kate Hunington</p>
                                    <p className="text-muted mb-0">kate.hunington@gmail.com</p>
                                </div>
                            </div>
                        </td>
                        <td>
                            <p className="fw-normal mb-1">Designer</p>
                            <p className="text-muted mb-0">UI/UX</p>
                        </td>
                        <td>
                            <span className="badge badge-warning rounded-pill d-inline">Awaiting</span>
                        </td>
                        <td>Senior</td>
                        <td>
                            <button
                                type="button"
                                className="btn btn-link btn-rounded btn-sm fw-bold"
                                data-mdb-ripple-color="dark"
                            >
                                Edit
                            </button>
                        </td>
                    </tr>
                </tbody>
            </table>
        </>
    );
}

export default Home;