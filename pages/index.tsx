import Head from 'next/head'
// import Image from 'next/image'
import { Inter } from '@next/font/google'
// import styles from '@/styles/Home.module.css'
import { GetStaticProps, NextPage } from 'next'
import Link from "next/link";
import { Welcome } from '../type'

//@ Components
import HomePageSearch from '@/components/home/HomePageSearch'
import Notification from '@/components/home/HomePageNotificationSection'
import HJobListingCard from '@/components/home/JobsListing'; // Home page Notification Section
import Header from "../components/layout/Header";
import setting from "../components/layout/setting.json";


import { useDispatch } from 'react-redux'
import { increment } from '../slices/counterSlices'
import { addMultiJob, } from '../slices/jobsSlice'
import { useEffect } from 'react'

const inter = Inter({ subsets: ['latin'] })

interface props {
  status?: "success",
  data: Welcome[]
}

export const getStaticProps: GetStaticProps<props> = async () => {
  try {
      const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/job`);
      if (!res.ok) {
          throw new Error(`Failed to fetch, status: ${res.status}`);
      }
      const { data }: props = await res.json();
      return {
          props: { data },
          revalidate: 600, // Revalidate every 10 minutes
      };
  } catch (error) {
      console.error('Error fetching data:', error);
      return {
          props: { data: [] }, // Handle error case
      };
  }
};

const Home: NextPage<props> = ({ data }) => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(addMultiJob(data))
  }, [dispatch, data])

  return (
    <>
      <Head>
        <title>Create Next Vivek</title>
        <meta name="description" content="Find Government jobs" />
        <meta httpEquiv="Cache-Control" content="max-age=600" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>
        <Header menu={setting.userHeader} />
        <HomePageSearch data={data} />
        {/* <button
          aria-label="Increment value"
          onClick={() => dispatch(increment())}
          style={{ marginTop: "100px" }}
        >
          Incrementk
        </button> */}
        {/* <Link href='./test'>Test</Link> */}
        <Notification data={data} name="Notification" />
        <HJobListingCard data={data} />
      </main>
    </>
  )
}
export default Home;