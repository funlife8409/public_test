import { NextApiRequest, NextApiResponse } from "next";
const Multer = require("multer");

// Configure Multer outside of the middleware
const upload:any = Multer({
    storage: Multer.memoryStorage(),
    limits: {
        fileSize: 5 * 1024 * 1024, // No larger than 5mb, change as you need
    },
});

export function myMiddleware(req: NextApiRequest, res: NextApiResponse, next: () => void) {
    // Example: Logging request details
    console.log(`Request Method: ${req.method}`);
    console.log(`Request URL: ${req.url}`);

    // Call next() to pass control to the next middleware in the stack
    next();
}

// Export the upload middleware for use in your API routes
export { upload };