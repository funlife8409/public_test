import { NextApiRequest, NextApiResponse } from "next";
import { Storage, StorageOptions } from "@google-cloud/storage";
import Multer from "multer";
import sharp from "sharp";
import { Welcome } from "../../../../type";
// import keyFile from "./Gl_Cloud_Key.json"
import { createApiErrorMessage, createApiSuccessMessage } from "../../../../globalFunction";

// const multer = Multer({
//     storage: Multer.memoryStorage(),
//     limits: {
//         fileSize: 5 * 1024 * 1024, // No larger than 5mb, change as needed
//     },
// });

let projectId = "primary-projects-396007"; // Get this from Google Cloud
const storage_key_env: string = process.env.G_CLOUD_STORAGE as string,
keyFilename = JSON.parse(storage_key_env);
// console.log();
// let keyFilename = "./G_Cloud_Key.json"; // Get this from Google Cloud -> Credentials -> Service Accounts
const storage = new Storage({
    projectId,
    keyFilename
});

export type props = {
    file?: any;
    status: "error" | "success";
    message?: String;
    data?: any;
}

const bucket = storage.bucket("primary_storage"); // Get this from Google Cloud -> Storage

// Configure multer for file uploads
const multerStorage = Multer.memoryStorage();
// const upload = Multer({ storage: multerStorage });


// Create an instance of Multer with desired storage settings
const upload = Multer({ storage: multerStorage });
export const config = {
    api: {
        bodyParser: false, // Disable Next.js built-in body parsing
    },
};

const image = async (req: NextApiRequest, res: NextApiResponse<props>) => {
    /* upload.single('file')(req as any, res as any, (err) => {
        if (err) {
            return res.status(500).json({ status: "error", message: err.message });
        }

        const { file }: any = req;
        // Define the destination path in the bucket
        const destination = 'uploads/' + file.originalname;

        // Upload the file to Google Cloud Storage
        const blob = bucket.file(destination);
        const blobStream = blob.createWriteStream();


        blobStream.on('error', (err) => {
            return res.status(500).send({ status: "error", message: `1. ${err.message}` });
        });

        blobStream.on('finish', () => {
            const publicUrl = `https://storage.googleapis.com/${bucket.name}/${blob.name}`;
            return res.status(200).send({ status: "success", message: `File uploaded successfully. View it <a href="${publicUrl}" target="_blank">here</a>.` });
        });

        blobStream.end(file.buffer);

        // You can process the uploaded file here and send a response
        // res.status(200).json({ status: "success", message: 'File uploaded successfully' });
    }); */
    return res.status(200).send({ status: "success", message: `File uploaded successfully.` });
}
export default image;