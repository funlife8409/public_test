import { NextApiRequest, NextApiResponse } from "next";
import clientPromise from '../../../../lib/mongodb';
// import { ObjectId } from "mongodb";
import { Welcome } from '../../../../type'
import { createApiErrorMessage, createApiSuccessMessage } from "../../../../globalFunction";

export type props = {
    file?: any;
    status: "error" | "success";
    message?: String;
    data?: any;
}

const saveJobPost = async (jobPost: any) => {
    const mongoClient = await clientPromise;
    
    try {
        const dublicateCheck = await mongoClient.db().collection('jobs').findOne({permalink: jobPost.permalink});
        if (!dublicateCheck) {
            const data = await mongoClient.db().collection('jobs').insertOne(jobPost);
            return { status: "success", data: jobPost.permalink};
        }
        return { status: "error", message: "dublicate post found!", data: {} };

    } catch (error) {
        return { status: "error", message: "Error adding post"};
    } finally {
        await mongoClient.close();
    }
};

const jobs = async (req: NextApiRequest, res: NextApiResponse<props>) => {
    if (req.method === 'POST') {
        try {
            const insert = await saveJobPost(req.body);
            if (insert.status === "success") {
                res.status(200).json(createApiSuccessMessage({status: "success", data: {}}));
            } else {
                res.status(500).json(createApiErrorMessage({status: "error", message: `${insert.message}`, data: {}}));
            }
        } catch (error) {
            res.status(500).json(createApiErrorMessage({ status: "error", message: `Error: ${error}`, data: null }));
        }
    } else {
        res.setHeader('Allow', ['POST']);
        res.status(405).end(`Method ${req.method} Not Allowed`);
    }
};

export default jobs;