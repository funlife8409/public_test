import { NextApiRequest, NextApiResponse } from "next";
import clientPromise from '../../../lib/mongodb';
import { createApiSuccessMessage } from "../../../globalFunction";
import { Welcome } from "../../../type";

type props = {
    status: "success",
    data: Welcome
}

export const getJob = async (req: NextApiRequest, res: NextApiResponse) => {
    const mongoClient = await clientPromise;
    const data = await mongoClient.db().collection('jobs').find({visibility: "public", status: "publish", permalink: req.query.jobSlug }).toArray();
    return data[0];
}

const jobD_by_Slug = async (req: NextApiRequest, res: NextApiResponse<props>) => {
    const data = await getJob(req, res)
    res.status(200).json(createApiSuccessMessage({status: "success", data: data}));
}
export default jobD_by_Slug;