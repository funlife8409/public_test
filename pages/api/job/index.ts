import { NextApiRequest, NextApiResponse } from "next";
import clientPromise from '../../../lib/mongodb';
// import { ObjectId } from "mongodb";
import { Welcome } from '../../../type'
import { createApiErrorMessage, createApiSuccessMessage } from "../../../globalFunction";

export type props = {
    status: "success",
    data: Welcome[];
}

export const getJob = async (): Promise<Welcome[]> => {
    const mongoClient = await clientPromise;
    const data = await mongoClient.db().collection('jobs').find().toArray();
    return JSON.parse(JSON.stringify(data))
    
}

const jobs = async (req: NextApiRequest, res: NextApiResponse<props>) => {
    const data = await getJob()
    res.status(200).json(createApiSuccessMessage({status: "success", data: data}));
}
export default jobs;