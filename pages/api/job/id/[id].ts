//! Please Convert this api to private-------

import { NextApiRequest, NextApiResponse } from "next";
import clientPromise from '../../../../lib/mongodb';
import { ObjectId } from 'mongodb';

type idd = { _id: string }
export const getJob = async (req: NextApiRequest, res: NextApiResponse) => {
    const mongoClient = await clientPromise;
    const id:any = req.query.id;
    const data = await mongoClient.db().collection('jobs').find({_id: new ObjectId(id) }).toArray();
    return data;
}       

const jobD_by_id = async (req: NextApiRequest, res: NextApiResponse<object>) => {
    const data = await getJob(req, res)
    res.status(200).json(data);
}
export default jobD_by_id;