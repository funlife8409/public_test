//? Coming Soon Template

import { NextApiRequest, NextApiResponse } from "next";
import Joi from "joi";
import clientPromise from '../../../lib/mongodb';
import { InsertOneResult, ObjectId } from "mongodb";
import { Welcome } from '../../../type'
import { stringify } from "querystring";
import Cors from 'cors';

export type Return = {
    jobs: Welcome[];
}
const cors = Cors({
    methods: ['POST'],
    origin: "*"
})

const schema = Joi.object({
    name: Joi.string().min(3).max(60).required(),
    email: Joi.string().email().min(5).max(300).required(),
    other: Joi.string().min(2).max(15).optional()
})

type addSubs = {
    insertedId: ObjectId | null;
    status: 'Success' | 'error';
    massage?: String;
}

// And to throw an error when an error happens in a middleware
function runMiddleware(
    req: NextApiRequest,
    res: NextApiResponse,
    fn: Function
  ) {
    return new Promise((resolve, reject) => {
      fn(req, res, (result: any) => {
        if (result instanceof Error) {
          return reject(result)
        }
  
        return resolve(result)
      })
    })
  }

//? Add Subscriber
export const addSubs = async (req: NextApiRequest): Promise<addSubs> => {
    const { name, email, other } = req.body;
    const mongoClient = await clientPromise;
    const data = await mongoClient.db().collection('subscriber');
    try {
        const value: { name: String, email: String, other: String } = await schema.validateAsync({ name, email, other });
        const fdata = await data.find({ email: email }).toArray();
        if (!fdata[0]) {
            const inst: InsertOneResult = await data.insertOne(value);
            return { insertedId: new ObjectId(inst.insertedId), status: 'Success' };
        } else {
            return { insertedId: null, status: 'error', massage: "you already subscribed" }
        }

    }
    catch (err: any) {
        return { insertedId: null, status: 'error', massage: err.details[0].message }
    }
}

const jobs = async (req: NextApiRequest, res: NextApiResponse<addSubs | String>) => {
    await runMiddleware(req, res, cors);
    const data = await addSubs(req)
    // if (req.method === 'POST') {
    if (data.status === 'Success') {
        res.status(200).json(data);
    } else {
        res.status(400).json(data);
    }
    // } else {
    //     res.status(400).send('invalid request');
    // }
}
export default jobs;