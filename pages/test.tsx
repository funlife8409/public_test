import React from 'react'
import { NextPage } from 'next'
import type { RootState } from '../store'
import { useSelector, useDispatch } from 'react-redux'
import {store} from '../store'

interface Props {
    count?: number;
}

const Page: NextPage<Props> = ({count}) => {
    return (
        <>
            <div><p>Your user agent: {count}</p></div>
        </>
    )
}

Page.getInitialProps = async ({ req }) => {
    let count = store.getState().counter.value;
    return { count }
}

export default Page