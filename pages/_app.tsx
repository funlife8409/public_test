import '@/styles/globals.css'
import '@/styles/fontawesome/css/fontawesome.css'
import '@/styles/fontawesome/css/solid.css'
// import '@/styles/googlefont.css'
import '@/styles/mdb.min.css'
import '@/styles/style.css'
import type { AppProps } from 'next/app'
import { store } from '../store'
import { Provider } from 'react-redux'

export default function App({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}> 
      <Component {...pageProps} />
    </Provider>
  )
}
