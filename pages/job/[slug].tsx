import Head from 'next/head'
import { NextPage } from 'next'
import { useDispatch } from 'react-redux'

//@ Components
import SJHeaderSection from '@/components/layout/SingleJobHeaderSection';
import SJContentSection from '@/components/layout/SJContentSection';
import Header from "../../components/layout/Header";
import setting from "../../components/layout/setting.json";

//@ Store & Slices
import { store } from '../../store'
import { increment } from '../../slices/counterSlices'
import { jobsInitState } from '@/slices/jobsSlice';

// typescript
import { Welcome } from '../../type'
interface props {
  data: Welcome;
  slug?: string;
}

var Home: NextPage<props> = ({ data, slug }) => {
  const dispatch = useDispatch();
  console.log(data, "check")
  return (
    <>
      <Head>
        <title>{data.title}</title>
        <meta name="description" content="Generated by create next app" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Header menu={setting.userHeader} />
      <main>
        <div style={{ backgroundColor: '#fff' }}>
          {/* {this.head()} */}
          <SJHeaderSection data={data} />

          <SJContentSection data={data} />
          {/* <ul>{this.renderUsers()}</ul> */}
        </div>
      </main>
    </>
  )
}

Home.getInitialProps = async ({ req, query }): Promise<props> => {
  let jobData: jobsInitState = store.getState().jobs as jobsInitState,
    slug = query.slug;
  // if job data is available in reducer transfer data without data fetching
  if (typeof slug === "string") {
    if (jobData[slug]) {
      return { data: jobData[slug] };
    }
  }
  // if job data is not available in reducer then fetch data
  const res = await fetch(`http://localhost:3000/api/job/${slug}`);
  const { data }: jobsInitState = await res.json();
  return { data }
}

export default Home;