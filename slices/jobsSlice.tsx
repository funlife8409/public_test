import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { Welcome } from "../type"

export interface jobsInitState {
    [key: string]: Welcome
}

const initialState: jobsInitState = {}
const multiJobs = (state: jobsInitState, data: Welcome[]): jobsInitState => {
    for (let i = 0; i < data.length; i++) {
        const element = data[i];
        state[element.permalink] = element;
    }
    return state;
}
const singleJobs = (state: jobsInitState, data: Welcome): jobsInitState => {
    state[data.permalink] = data;
    return state;
}

export const counterSlice = createSlice({
    name: 'counter',
    initialState,
    reducers: {
        addMultiJob: (state, action: PayloadAction<Welcome[]>) => {
            return multiJobs(state, action.payload)
        },
        addSingleJob: (state, action: PayloadAction<Welcome>) => {
            return singleJobs(state, action.payload)
        }
    },
})

// Action creators are generated for each case reducer function
export const { addMultiJob, addSingleJob } = counterSlice.actions

export default counterSlice.reducer