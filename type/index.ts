import { ObjectId } from "mongodb";

export interface AllJobs {
    data: Welcome[];
}

export interface Welcome {
    _id: ObjectId;
    basic_detail: BasicDetail;
    updateUser: any[];
    updateDate: any[];
    author: string;
    soc_media: SocMedia;
    dates: Dates;
    links: Links;
    application_fee: ApplicationFee;
    age: Age;
    tags: Tags;
    elegibility: string;
    description: string;
    visibility: string;
    status: string;
    publishDate: Date;
    permalink: string;
    date: Date;
    __v: number;
    logo?: string;
    title: string;
    type: string;
    sortDetail?: string;
}

export interface NewJob {
    basic_detail:    BasicDetail;
    soc_media:       SocMedia;
    dates:           Dates;
    links:           Links;
    application_fee: ApplicationFee;
    age:             Age;
    tags:            Tags;
    title:           string;
    elegibility:     string;
    description:     string;
    sortDetail:      string;
    visibility:      string;
    status:          string;
    type:            string;
    publishDate:     Date;
    permalink:       string;
}

export interface Age {
    minWom: string;
    maxWom: string;
    minMen: string;
    maxMen: string;
    ageRelax: string;
    other: Array<string[]>;
}

export interface ApplicationFee {
    [key: string]: string | undefined;
    gen: string;
    obc: string;
    sc: string;
    st: string;
    phCand: string;
    //@ts-ignore
    other: string[][];
}

export interface BasicDetail {
    availability: Availability[];
    PName: string;
    secondry_name: string;
    Orgnization: string;
    compny_name: string;
    total_vacancy: string;
    minAge: string;
    maxAge: string;
    logo: string;
    compny_type?: string;
}

export enum Availability {
    Empty = "",
    Full = "Full",
    PartTime = "Part Time",
}

export interface Dates {
    [key: string]: string | undefined;
    aplBegin: string;
    lDate: string;
    lDatePayExFee: string;
    exmDate: string;
    admCard: string;
    //@ts-ignore
    other: string[][];
}

export enum Other {
    Empty = "",
    ReUploadPhotoSignature = "Re Upload Photo / Signature",
    The1118January2023 = "11-18 January 2023",
}

export interface Links {
    [key: string]: string | undefined;
    apyLink: string;
    downNotifLink: string;
    offWebLink: string;
    downAdmitLink: string;
    viewRes: string;
    // @ts-ignore
    other: string[][];
}

export interface SocMedia {
    fb: string;
    wb: string;
    ph: string;
    em: string;
    ofw: string;
    twet: string;
    lnkdin: string;
    gMap: string;
    other: Array<string[]>;
}

export interface Tags {
    tags: Tag[];
    other: string[];
}

export enum Tag {
    Bca = "BCA",
    Empty = "",
    Graduate = "Graduate",
    Mbbs = "MBBS",
    Medical = "Medical",
    SportsMan = "Sports Man",
}

// Header Section Types

export interface Navbar {
    userHeader: HeaderS;
    adminHeader: HeaderS;
}

export interface HeaderS {
    logoImg: string;
    logoName: string;
    links: Array<string[]>;
}
