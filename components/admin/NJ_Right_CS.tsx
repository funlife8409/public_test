/**
 * @NJ_Left_CS NewJob.js Right Content Section
 */

import React, { useState } from "react";

const Home = () => {
    return (
        <div className="row">
            <div className="col-12">
                <ul className="nav nav-tabs mb-4" id="ex1" role="tablist">
                    <li className="nav-item" role="presentation">
                        <a className="py-2 nav-link active" id="ex1-tab-1" data-mdb-toggle="tab" href="#ex1-tabs-1" role="tab" aria-controls="ex1-tabs-1" aria-selected="true">Tab 1</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="py-2 nav-link" id="ex1-tab-2" data-mdb-toggle="tab" href="#ex1-tabs-2" role="tab" aria-controls="ex1-tabs-2" aria-selected="false" >Tab 2</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="py-2 nav-link" id="ex1-tab-3" data-mdb-toggle="tab" href="#ex1-tabs-3" role="tab" aria-controls="ex1-tabs-3" aria-selected="false" >Tab 3</a>
                    </li>
                </ul>

                <div className="tab-content" id="ex1-content">
                    <div className="tab-pane fade show active" id="ex1-tabs-1" role="tabpanel" aria-labelledby="ex1-tab-1" >
                        <div className="row">
                            <div className="col-12">
                                <div className="input-group mb-3">
                                    <input type="text" className="form-control" placeholder="Type keyword / Url" aria-label="Recipient's username" aria-describedby="button-addon2" />
                                    <button className="btn btn-primary" type="button" id="button-addon2" data-mdb-ripple-color="dark">
                                        Search
                                    </button>
                                </div>
                            </div>
                            <div className="col-12">
                                <iframe src="https://www.example.com/" height="100%" width="100%"></iframe>
                            </div>
                        </div>
                    </div>
                    <div className="tab-pane fade" id="ex1-tabs-2" role="tabpanel" aria-labelledby="ex1-tab-2">
                        Tab 2 content
                    </div>
                    <div className="tab-pane fade" id="ex1-tabs-3" role="tabpanel" aria-labelledby="ex1-tab-3">
                        Tab 3 content
                    </div>
                </div>
            </div>


            {/* <div className="col-2">
                <button type="submit" className="btn btn-primary">Submit</button>
            </div> */}

        </div>
    );
};
export default Home;