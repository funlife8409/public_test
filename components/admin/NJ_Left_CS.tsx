/**
 * @NJ_Left_CS NewJob.js Left Content Section
*/

import React, { useState, useRef, InputHTMLAttributes, FormEvent, useEffect } from "react";
import axios, { AxiosError, AxiosResponse } from "axios";
import MultiSelect from '../layout/MultiSelect'
import Input from '../layout/Input'
import OtherOption from '../layout/OtherOption'
import OtherSingleOption from '../layout/OtherSingleOption'

//@ Components
import { Welcome, NewJob } from "../../type";
type tryError = {
    error: {
        response: {
            data: string;
        }
    }
}

interface newPostAdd {

}

// Functitons
async function newPost(data: any) {
    var authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmY2NjliYmEwMTJjZjAyYzQ4NTY3MzUiLCJpYXQiOjE2MDk5ODQ0Nzl9.M5znzRnBaqOpd0jySILXeiUmjZHKvJ93PHxVjG3xG7E";

    try {
        const response = await axios.post('http://localhost:3000/api/admin/job/new-post', data, { headers: { 'auth-token': authToken } });
        return response.data;
    } catch (err: any) {
        throw err.response?.data;
    }
}

async function upoadImg(file: any) {
    const imgformData = new FormData();
    imgformData.append('image', file);
    var authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZmY2NjliYmEwMTJjZjAyYzQ4NTY3MzUiLCJpYXQiOjE2MDk5ODQ0Nzl9.M5znzRnBaqOpd0jySILXeiUmjZHKvJ93PHxVjG3xG7E";
    try {
        const response = await axios.post('http://localhost:3000/api/admin/upload/image?type=original', imgformData, { headers: { 'auth-token': authToken } });
        return response.data;
    } catch (error: any) {
        throw error.response.data;
    }
    // await axios.post('http://localhost:4000/api/admin/upload/image', imgformData, {
    //     headers: {
    //         'auth-token': authToken
    //     }
    // }).then(response => {
    //     return response.data;
    // }).catch(error => {
    //     return {status: "error"}
    // });
    // return;
}

const Home = () => {
    const avRef = useRef<HTMLFormElement>(null);
    const [logo, setLogo] = useState(null)
    const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        let postDat = {
            title: "",
            basic_detail: {
                PName: "",
                secondry_name: "",
                Orgnization: "",
                compny_name: "",
                total_vacancy: "",
                minAge: "",
                maxAge: "",
                logo: "",
                availability: [""],
            },
            soc_media: {
                fb: "",
                wb: "",
                ph: "",
                em: "",
                ofw: "",
                twet: "",
                lnkdin: "",
                gMap: "",
                other: [[""]]
            },
            dates: {
                aplBegin: "",
                lDate: "",
                lDatePayExFee: "",
                exmDate: "",
                admCard: "",
                other: [[""]],
            },
            application_fee: {
                gen: "",
                obc: "",
                sc: "",
                st: "",
                phCand: "",
                other: [[""]],
            },
            links: {
                apyLink: "",
                downNotifLink: "",
                offWebLink: "",
                downAdmitLink: "",
                viewRes: "",
                other: [[""]],
            },
            age: {
                minWom: "",
                maxWom: "",
                minMen: "",
                maxMen: "",
                ageRelax: "",
                other: [[""]],
            },
            tags: {
                tags: [""],
                other: [""]
            },
            elegibility: "",
            description: "",
            sortDetail: "",
            visibility: "",
            status: "",
            type: "",
            publishDate: "",
            permalink: ""
        };
        // const data = new FormData(e.target);
        // const value = Object.fromEntries(data.entries());
        try {
            let formDat = avRef.current!;
            let bsDtls = postDat['basic_detail'];
            let socDtls = postDat['soc_media'];
            let impDates = postDat['dates'];
            let appliFee = postDat['application_fee'];
            let link = postDat['links'];
            let tags = postDat['tags'];
            let age = postDat['age'];
            let getMultiSelectVal = (name: string): string[] => {
                if (formDat) {
                    return [...formDat[name]].map(v => v.selected ? v.value : "")
                } else {
                    return []
                }
            };
            let OtherSingleOption = (name: string): string[] => {
                const elements = Array.from(document.querySelectorAll(`input[id^=${name}]`)) as HTMLInputElement[];
                const regex = /\d+/g;
                let groupInputLength = 0;
                let groupInputs: string[] = [];
                elements.forEach(v => {
                    let str = v.name;
                    const idMatch = str.match(regex);
                    if (idMatch !== null) {
                        const id = parseInt(idMatch[0]);
                        if (!isNaN(id)) {
                            groupInputLength = id;
                        }
                    }
                })
                for (let i = 0; i < groupInputLength + 1; i++) {
                    let il;
                    if (i === 0) {
                        il = document.querySelector(`input#${name}`);
                    } else if (i > 0) {
                        il = document.querySelector(`input#${name}_${i}`);
                        // groupInputs.push([il.value, ir.value])
                    }
                    if (il && 'value' in il) {
                        const ilElement = il as HTMLInputElement;
                        groupInputs.push(ilElement.value)
                    }
                }
                return groupInputs;
            }
            let groupMultiInput = (groupId = "") => {
                // let sAi = "input[id^='soc']"; // select all input
                const elements = Array.from(document.querySelectorAll(`input[id^=${groupId}]`)) as HTMLInputElement[];
                const regex = /\d+/g;
                let groupInputLength = 0;
                let groupInputs = [];
                elements.forEach(v => {
                    let str = v.name;
                    const idMatch = str.match(regex);
                    if (idMatch) {
                        let id = parseInt(idMatch[0]);
                        if (!isNaN(id)) {
                            groupInputLength = id;
                        }
                    }
                })
                for (let i = 0; i < groupInputLength + 1; i++) {
                    let il, ir;
                    if (i === 0) {
                        il = document.querySelector(`input#${groupId}`);
                        ir = document.querySelector(`input#${groupId}_val`);
                    } else if (i > 0) {
                        il = document.querySelector(`input#${groupId}_${i}`);
                        ir = document.querySelector(`input#${groupId}_${i}_val`);
                        // groupInputs.push([il.value, ir.value])
                    }
                    if (il || ir) {
                        const ilElement = il as HTMLInputElement;
                        const irElement = ir as HTMLInputElement;
                        groupInputs.push([ilElement.value, irElement.value])
                    }
                }
                return groupInputs;
            }

            // Basic Details
            postDat['title'] = formDat.PTitle.value;
            bsDtls['PName'] = formDat['PName'].value;
            bsDtls['secondry_name'] = formDat['SName'].value;
            bsDtls['Orgnization'] = formDat['Org'].value;
            bsDtls['compny_name'] = formDat['CName'].value;
            bsDtls['total_vacancy'] = formDat['TVacancy'].value;
            bsDtls['minAge'] = formDat['MinAge'].value;
            bsDtls['maxAge'] = formDat['MaxAge'].value;
            bsDtls['availability'] = getMultiSelectVal('availability');

            // Social Media
            socDtls['fb'] = formDat['fb'].value;
            socDtls['wb'] = formDat['wb'].value;
            socDtls['ph'] = formDat['ph'].value;
            socDtls['em'] = formDat['em'].value;
            socDtls['ofw'] = formDat['ofw'].value;
            socDtls['twet'] = formDat['twet'].value;
            socDtls['lnkdin'] = formDat['lnkdin'].value;
            socDtls['gMap'] = formDat['gMap'].value;
            socDtls['gMap'] = formDat['logo'].value;
            socDtls['other'] = groupMultiInput("soc");

            // Important Dates
            impDates['aplBegin'] = formDat['aplBegin'].value;
            impDates['lDate'] = formDat['lDate'].value;
            impDates['lDatePayExFee'] = formDat['lDatePayExFee'].value;
            impDates['exmDate'] = formDat['exmDate'].value;
            impDates['admCard'] = formDat['admCard'].value;
            impDates['other'] = groupMultiInput("impDate");

            // Applicatiton Fee
            appliFee['gen'] = formDat['gen'].value;
            appliFee['obc'] = formDat['obc'].value;
            appliFee['sc'] = formDat['sc'].value;
            appliFee['st'] = formDat['st'].value;
            appliFee['phCand'] = formDat['phCand'].value;
            appliFee['other'] = groupMultiInput("fee");

            // Links
            link['apyLink'] = formDat['apyLink'].value;
            link['downNotifLink'] = formDat['downNotifLink'].value;
            link['offWebLink'] = formDat['offWebLink'].value;
            link['downAdmitLink'] = formDat['downAdmitLink'].value;
            link['viewRes'] = formDat['viewRes'].value;
            link['other'] = groupMultiInput("link");

            // Age
            age['minWom'] = formDat['minWom'].value;
            age['maxWom'] = formDat['maxWom'].value;
            age['minMen'] = formDat['minMen'].value;
            age['maxMen'] = formDat['maxMen'].value;
            age['ageRelax'] = formDat['ageRelax'].value;
            age['other'] = groupMultiInput("age");

            // Elegibility
            //@ts-ignore
            postDat['elegibility'] = CKEDITOR.instances.elegibility.getData();

            // Tags
            tags['tags'] = getMultiSelectVal('tags');
            tags['other'] = OtherSingleOption('tag');

            // Description
            //@ts-ignore
            postDat['description'] = CKEDITOR.instances.descBlog.getData();

            // Short Detail
            //@ts-ignore
            postDat['sortDetail'] = CKEDITOR.instances.sortDetail.getData();

            // Other Requard Value
            postDat['visibility'] = formDat.visibility.value;
            postDat['status'] = formDat.status.value;
            postDat['type'] = formDat.type.value;
            postDat['publishDate'] = formDat.pub_Date.value;
            postDat['permalink'] = formDat.permalink.value;

            const form = e.target as HTMLFormElement;
            const logoFile = form.logo.files[0];
            if(logoFile) {
                upoadImg(logoFile).then((v) => {
                    setLogo(v.data.fileName);
                    bsDtls['logo'] = v.data.fileName;
                    newPost(postDat).then((v) => console.log(v)).catch((err) => alert(err));
                }).catch((err) => alert(`vivek ${err.message}`));
            }else {
                // bsDtls['logo'] = logo;
                newPost(postDat).then((v) => console.log(v)).catch((err) => alert(err));
            }
            /* if (!logo) {
                // console.warn(logoFile, "Vhek");
                upoadImg(logoFile).then((v) => {
                    setLogo(v.data.fileName);
                    bsDtls['logo'] = v.data.fileName;
                    newPost(postDat).then((v) => console.log(v)).catch((err) => alert(err));
                }).catch((err) => alert(`vivek ${err.message}`));
            } else {
                bsDtls['logo'] = logo;
                newPost(postDat).then((v) => console.log(v)).catch((err) => alert(err));
            } */


        } catch (err: any) {
            console.warn(err.message);
        }
    };
    const toggle = () => {
        //@ts-ignore
        $('.card-body').toggleClass('show');
    }
    useEffect(() => {
        //@ts-ignore
        $(document).ready(function() {
            //@ts-ignore
            CKEDITOR.replace( 'elegibility' );
            //@ts-ignore
            CKEDITOR.replace( 'descBlog' );
            //@ts-ignore
            CKEDITOR.replace( 'sortDetail' );
            //@ts-ignore
            MultiselectDropdown(window.MultiselectDropdownOptions);
        });
    }, [])
    return (
        // New Job Left Section
        <div className="NJLCS">
            <form onSubmit={handleSubmit} ref={avRef}>
                <div className="row mb-3">
                    <div className="col-4"><h4>Add New Job </h4></div>
                    <div className="col">

                        <div className="btn-toolbar ms-auto" role="toolbar" style={{ display: "table" }}>
                            <div className="btn-group me-2" role="group" aria-label="First group">
                                <input type="date" id="pub_Date" name="pub_Date" style={{ padding: "4.5% 3.5%", border: "none", borderRadius: "7%" }} />
                            </div>
                            <div className="btn-group me-2" role="group" aria-label="Second group">
                                <select defaultValue="public" className="form-select" name="visibility" aria-label="Default select example">
                                    <option value="public">public</option>
                                    <option value="private">Private</option>
                                </select>
                                <select className="form-select" defaultValue="draft" name="status" aria-labelledby="btnGroupDrop1">
                                    <option className="dropdown-item" value="draft">Draft</option>
                                    <option className="dropdown-item" value="publish">Publish</option>
                                    <option className="dropdown-item" value="trash">Trash</option>
                                    <option className="dropdown-item" value="bin">Bin</option>
                                </select>
                            </div>
                            <div className="btn-group" role="group" aria-label="Third group">
                                <button type="submit" className="btn btn-success">Submit</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div className="form-outline">
                    <input type="text" id="PTitle" name="PTitle" className="form-control border bg-white" />
                    <label className="form-label" htmlFor="PTitle">Primary Title</label>
                </div>
                <div className="form-outline mt-4">
                    <input type="text" id="permalink" name="permalink" className="form-control border bg-white" />
                    <label className="form-label" htmlFor="permalink">Slug / Permalink</label>
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                        Basic Detail
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="collapseExample">
                        <div className="row">
                            <div className="col-6">
                                <Input id="PName" name="PName" title="Post Name" />
                            </div>
                            <div className="col-6">
                                <Input id="SName" name="SName" title="Secondry Name (2-3 Word)" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="CName" name="CName" title="Compny Name" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="Org" name="Org" title="Orgnization" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="TVacancy" name="TVacancy" type="number" title="Total Vacancy" min={1} max={1000000} />
                            </div>
                            <div className="col-6 mt-4">
                                <MultiSelect name="availability" id="av" select={[0, 1]} value={["Full", "Part Time"]} onChange={() => { "console.log(this.selectedOptions)" }} />
                            </div>
                            <div className="col-6 mt-4">
                                <label htmlFor="formFile" className="form-label mt-3" ></label>
                                <Input id="MinAge" name="MinAge" type="number" title="Min Age" min={1} max={90} />
                            </div>
                            <div className="col-6 mt-4">
                                <label htmlFor="formFile" className="form-label mt-3" ></label>
                                <Input id="MaxAge" name="MaxAge" type="number" title="Max Age" min={1} max={90} />
                            </div>
                            <div className="col-6 mt-4">
                                <select className="form-select" name="type" aria-label="Default select example">
                                    <option defaultValue="government">Governent</option>
                                    <option value="private">Private</option>
                                </select>
                            </div>
                            <div className="col-6 mt-4">
                                <div className="mb-3">
                                    <input className="form-control" type="file" name="logo" id="formFile" />
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#desc" aria-expanded="false" aria-controls="desc">
                        Short Detail
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="desc">
                        <div className="row">
                            <div className="col">
                                <div className="form-outline">
                                    <textarea className="form-control" name="sortDetail" id="sortDetail" rows={10}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#soc" aria-expanded="false" aria-controls="soc">
                        Social Media
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="soc">
                        <div className="row">
                            <div className="col-6">
                                <Input id="fb" name="fb" title="Facebook" />
                            </div>
                            <div className="col-6">
                                <Input id="wb" name="wb" title="Whatsapp" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="ph" name="ph" title="Phone" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="em" name="em" title="Email" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="ofw" name="ofw" title="Official Website" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="twetr" name="twet" title="Twitter" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="lnkdin" name="lnkdin" title="Linkedin" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="gMap" name="gMap" title="Map Address" />
                            </div>
                            <OtherOption name="soc" placeholderName={["Other Title", "Other Value"]} />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#impDates" aria-expanded="false" aria-controls="impDates">
                        Important Dates
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="impDates">
                        <div className="row">
                            <div className="col-6">
                                <Input id="aplBegin" name="aplBegin" title="Application Begin" />
                            </div>
                            <div className="col-6">
                                <Input id="lDate" name="lDate" title="Last Date" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="lDatePayExFee" name="lDatePayExFee" title="Last date pay exam fee" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="exmDate" name="exmDate" title="Exam Date" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="admCard" name="admCard" title="Admit Card" />
                            </div>
                            <OtherOption name="impDate" placeholderName={["Date Title", "Date Value"]} />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#appFee" aria-expanded="false" aria-controls="appFee">
                        Application Fee
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="appFee">
                        <div className="row">
                            <div className="col-6">
                                <Input id="gen" name="gen" title="General" />
                            </div>
                            <div className="col-6">
                                <Input id="obc" name="obc" title="OBC" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="sc" name="sc" title="SC" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="st" name="st" title="ST" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="phCand" name="phCand" title="PH Candidates" />
                            </div>
                            <div className="col-6 mt-4">
                                {/* <MultiSelect name="payType" id="payType" select={[2, 3, 4]} value={["Debit", "Other", "Upi", "Credit", "Net Banking", "Postal", "On Branch"]} onChange={() => { "console.log(this.selectedOptions)" }} /> */}
                            </div>
                            <OtherOption name="fee" placeholderName={["Fee Title", "Fee Value"]} />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#links" aria-expanded="false" aria-controls="links">
                        Links
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="links">
                        <div className="row">
                            <div className="col-6">
                                <Input id="apyLink" name="apyLink" title="Apply Online" />
                            </div>
                            <div className="col-6">
                                <Input id="downNotifLink" name="downNotifLink" title="Download Notification" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="offWebLink" name="offWebLink" title="Official Website" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="downAdmitLink" name="downAdmitLink" title="Download Admit Card" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="viewRes" name="viewRes" title="View Result" />
                            </div>
                            <OtherOption name="link" placeholderName={["Link Title", "Link Value"]} />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#ages" aria-expanded="false" aria-controls="ages">
                        Age
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="ages">
                        <div className="row">
                            <div className="col-6">
                                <Input id="minWom" name="minWom" title="Women Min" />
                            </div>
                            <div className="col-6">
                                <Input id="maxWom" name="maxWom" title="Women Max" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="minMen" name="minMen" title="Men Min" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="maxMen" name="maxMen" title="Men Max" />
                            </div>
                            <div className="col-6 mt-4">
                                <Input id="ageRelax" name="ageRelax" title="Age Relaxation" />
                            </div>
                            <OtherOption name="age" placeholderName={["Age Title", "Age Value"]} />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#elegibilityTog" aria-expanded="false" aria-controls="elegibilityTog">
                        Elegibility
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse show" id="elegibilityTog">
                        <div className="row">
                            <div className="col">
                                <div className="form-outline">
                                    <textarea className="form-control" name="elegibility" id="elegibility" rows={10}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#postTag" aria-expanded="false" aria-controls="postTag">
                        Tags
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="postTag">
                        <div className="row">
                            <div className="col">
                                <div className="form-outline">
                                    <MultiSelect name="tags" id="tags" select={[2, 3, 4]} value={["Matric", "BA", "Graduate", "Sports Man", "BCA", "B.Tec", "MCA"]} onChange={() => { "console.log(this.selectedOptions)" }} />
                                </div>
                            </div>
                            <OtherSingleOption name="tag" id="tag" />
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
                <div className="card mt-4 collapse-card">
                    <div className="card-header" onClick={toggle} data-mdb-toggle="collapse" data-mdb-target="#desc" aria-expanded="false" aria-controls="desc">
                        Description / Blog
                        <span className="float-end"><i className="fas fa-chevron-down"></i></span>
                    </div>
                    <div className="card-body collapse" id="desc">
                        <div className="row">
                            <div className="col">
                                <div className="form-outline">
                                    <textarea className="form-control" name="descBlog" id="descBlog" rows={10}></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* <div className="card-footer text-muted">2 days ago</div> */}
                </div>
            </form>
        </div>
    );
};
export default Home;