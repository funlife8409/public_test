import React from 'react';
import HJobListingCard from '../layout/HJobListingCard';

// import types
import { Welcome } from '../../type'

interface props {
    data: Welcome[];
}

const JobsListing = ({ data }:props) => {
    return (
        <div className="HListingCard">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Result" data={data}/>
                    </div>
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Admit Card" data={data}/>
                    </div>
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Latest Jobs" data={data}/>
                    </div>
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Answer Key" data={data}/>
                    </div>
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Syllabus" data={data}/>
                    </div>
                    <div className="col-md-4 col-sm-6">
                        <HJobListingCard name="Admission" data={data}/>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default JobsListing