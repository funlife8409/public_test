import { Inter } from '@next/font/google'
import Link from "next/link";
import Image from "next/image";
import { Welcome } from '../../type'

//section components
import T2I from '../layout/text2CircleImage'

const inter = Inter({ subsets: ['latin'] })
type right = {
  name: string | null;
  value: string | null;
}
type left = {
  name: string | null;
  value: string | null;
}

export default function HomePageSearch({ data }: { data: Welcome }) {
  const leftDate = (): left => {
    var obj: { name: string | null, value: string | null } = { name: null, value: null };
    if (data.publishDate) {
      obj.name = "Post Update:- ",
        obj.value = data.publishDate.toString()
    }
    return obj;
  };
  const rightDate = ():right => {
    var obj: { name: string | null, value: string | null } = { name: null, value: null }

    switch (data.type) {
      case "admitcard":
        obj.name = "Result Declared:- ";
        obj.value = data.dates.exmDate
      case "latest":
        obj.name = "Last Date:- ";
        obj.value = data.dates.lDate
      default:
        obj.name = null;
        obj.value = null
    }
    return obj;
  };
  const logo = () => {
    if (data.logo)
      return <Image src={data.logo} alt={data.permalink} width={45} height={45}/>
    else
      return <T2I value={data.title[0].toUpperCase()} />
  }
  const url = () => {
    return "/job/" + data.permalink;
  }
  return (
    <div>
      <div className="card shadow-0">
        <div className="card-body row jobListingCard d-flex align-items-center justify-content-center">
          <div className="col-2 cardLogo text-center"><Link href={url()}>{logo()}</Link></div>
          <div className="col cardContent">
            <h5 className="card-title"><Link href={url()}>{data.title}</Link></h5>
            <p className="m-0"> <b>{leftDate().name} </b>{leftDate().value}&nbsp; <b> {rightDate().name} </b>{rightDate().value}</p>
          </div>
        </div>
      </div>
    </div>
  );
}