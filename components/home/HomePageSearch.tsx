import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import { Welcome } from '../../type'

const inter = Inter({ subsets: ['latin'] })

export default function HomePageSearch({ data }: { data: Welcome[] }) {
  return (
    <>
      <div className="bg-primary homePageSearchSection">
        <div id="bg-image">
          <div className="container pb-5">
            <div className="row py-5">
              <div className="col-md-2 d-md-none d-lg-block"></div>
              <div className="col-md-auto mx-auto text-center display-6">
                <label htmlFor="search" className="form-label py-4 text-white">Find Your Jobs & Colleges Here!</label>
                <div className="input-group">
                  <button type="button" className="btn btn-light">
                    <i className="fas fa-search"></i>
                  </button>
                  <input type="email" className="form-control" id="search" placeholder="Search Jobs!" />
                </div>
              </div>
              <div className="col-md-2 d-md-none d-lg-block"></div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
