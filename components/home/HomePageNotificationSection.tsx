import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import { Welcome } from '../../type'

import JobListing from './JobListing'
import { useEffect, useRef } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function HomePageSearch({ data, name }: { data: Welcome[], name: string }) {
    var jobNoti = data.map((v, i) =>
        <div className="col-sm-6 col-lg-4" key={i}>
            {/* {console.log("val", val)} */}
            <JobListing data={v} />
        </div>
    )
    useEffect(() => {
        //? Notification Tab manager
        document.querySelectorAll(".notification ul li").forEach(element => {
            element.addEventListener('click', function (this: HTMLElement) {
                const name = this.getAttribute('role');
                if (name) {
                    let tb: HTMLDivElement = document.getElementById(name) as HTMLDivElement;
                    let tabs = ["tab1", "tab2", "tab3"];
                    tabs.forEach((v) => {
                        if (name === v) {
                            tb.classList.add("show", "active");
                            document.querySelectorAll(".notification ul li").forEach(element => {
                                element.querySelector("p")?.classList.remove("active")
                            })
                            this.querySelector(".nav-link")?.classList.add("active");
                        }
                        else {
                            document.getElementById(v)?.classList.remove("show", "active");
                        }
                    })
                }
            });
        })
    }, [])

    return (
        <div className="notification">
            <div className="container-fluid border pt-2 px-4">
                <div className="row">
                    <div className="col-2 pt-2"><b>{name}</b></div>
                    <div className="col-10">
                        {/* Tab Navigation */}
                        <ul className="nav nav-tabs d-flex justify-content-end" id="ex1" role="tablist">
                            <li className={`nav-item ${styles.notificationNav}`} role="tab1">
                                <p
                                    className="nav-link active"
                                    id="ex1-tab-1"
                                    data-mdb-toggle="tab"
                                >Jobs</p>
                            </li>
                            <li className={`nav-item ${styles.notificationNav}`} role="tab2">
                                <p
                                    className="nav-link"
                                    id="ex1-tab-2"
                                    data-mdb-toggle="tab"
                                >Expired Jobs</p>
                            </li>
                            <li className={`nav-item ${styles.notificationNav}`} role="tab3">
                                <p
                                    className="nav-link"
                                    id="ex1-tab-3"
                                    data-mdb-toggle="tab"
                                >UpComing Jobs</p>
                            </li>
                        </ul>
                        {/* Tab Navigation End*/}
                    </div>
                </div>
            </div>
            {/* Tab Content Section */}
            <div className="container-fluid px-4">
                <div className="row">
                    <div className="col-12">
                        <div className="tab-content py-4" id="ex1-content">
                            <div
                                className="tab-pane fade show active"
                                id="tab1"
                                role="tabpanel"
                                aria-labelledby="ex1-tab-1"
                            >
                                <div className="row">
                                    {jobNoti}
                                </div>
                            </div>
                            <div className="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="ex1-tab-2">
                                Tab 2 content
                            </div>
                            <div className="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="ex1-tab-3">
                                Tab 3 content
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}