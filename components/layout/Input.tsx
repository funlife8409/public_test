import React from 'react';
type props = {
    type?: string,
    title?: string,
    value?:string,
    name?: string,
    id?: string,
    onChange?: any,
    min?: number,
    max?: number
}
const Home = ({ type= "text", title = "Your Title", value, name = "", id = "", onChange, min, max }: props) => {
    return (
        <div className="form-outline">
            <input type={type} id={id} className="form-control border bg-white" value={value} name={name} onChange={onChange}  min={min} max={max}/>
            <label className="form-label" htmlFor={id}>{title}</label>
        </div>
    );
};
export default Home;