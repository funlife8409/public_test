import React from 'react';
import Link from "next/link";
import { HeaderS } from '../../type';
/**
 * @menus {
    logoImg: "",
    logoName: "ResultLo",
    links: [
      ["to", "title"],
      ["to", "title"]
    ]
  }
 */
interface HeaderProps {
    menu: HeaderS;
}
const Header = ({ menu }: HeaderProps) => {
    console.log("props", menu);
    // const menu = {
    //   logoImg: "",
    //   logoName: "ResultLo",
    //   links: [
    //     ["to", "title"],
    //     ["to", "title"]
    //   ]
    // }
    // const authButton = auth ? (
    //   <a className="nav-link px-3" href="/api/logout"><i className="icon-thin-user px-2 fa-md fw-bold"></i>Logout</a>
    // ) : (
    //     <a className="nav-link px-3" href="/api/auth/google"><i className="icon-thin-user px-2 fa-md fw-bold"></i>Login</a>
    //   );

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container-fluid">
                <button
                    className="navbar-toggler"
                    type="button"
                    data-mdb-toggle="collapse"
                    data-mdb-target="#navbarTogglerDemo03"
                    aria-controls="navbarTogglerDemo03"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                >
                    <i className="fas fa-bars"></i>
                </button>
                <Link href="/" className="navbar-brand brand-logo" aria-current="page">
                    {menu.logoName}
                </Link>
                <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
                    <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                        {
                            menu.links.map((d, i) => {
                                var cN = d[1] + " px-2 fw-bold"; // Class Name
                                return (<li className="nav-item" key={i}>
                                    <Link className="nav-link px-3" href={d[0]}><i className={cN}></i>{d[2]}</Link>
                                </li>)
                            })
                        }
                        {/* <li className="nav-item">
              <Link className="nav-link px-3" to="/jobs"><i className="icon-thin-info px-2 fa-md fw-bold"></i>Latest Jobs</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/college"><i className="icon-thin-mail-closed px-2 fa-md fw-bold"></i>College</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/results"><i className="icon-thin-contact-card px-2 fa-md fw-bold"></i>Results</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/admit-card"><i className="icon-thin-newspaper px-2 fa-sm fw-bold"></i>Admit Card</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/answer-key"><i className="icon-thin-briefcase px-2 fa-sm fw-bold"></i>Answer Key</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/users"><i className="icon-thin-briefcase1 px-2 fa-sm fw-bold"></i>Users</Link>
            </li>
            <li className="nav-item">
              <Link className="nav-link px-3" to="/admins"><i className="icon-thin-mail-closed px-2 fa-md fw-bold"></i>Admins</Link>
            </li> */}
                        {/* </ul>
          <ul className="navbar-nav me-auto mb-lg-0"> */}
                        <li className="nav-item">
                            {/* {authButton} */}
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};

export default Header;