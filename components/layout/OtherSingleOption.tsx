import React, { useState } from 'react';
import Input from './Input'
type props = {
    name?: string,
    id?: string,
    onChange?: any,
    placeholderName?: string[],
}
const Home = ({ name = "", id = "", onChange, placeholderName = ["Other Title", "Other Value"] }:props) => {
    const [socName, setSocName] = useState<string[]>([]);
    const moreOption = (e:string) => {
        setSocName([...socName, e + "_" + (socName.length + 1)]);
    }
    let appendOpt = function (X:string, v:number) {
        return <div className="col-6 mt-4" key={v}>
            <Input id={X} name={X} title={placeholderName[0] + " - " + (v + 1)} />
        </div>;
    };
    return (
        <React.Fragment>
            <div className="col-6">
                <div className="row">
                    <div className="col pe-0">
                        <Input id={name} name={name} title={placeholderName[0]} />
                    </div>
                    <div className="col-3 ps-0">
                        <button type="button" className="btn btn-primary" onClick={() => moreOption(name)}>+</button>
                    </div>
                </div>
            </div>
            {socName.map(appendOpt)}
        </React.Fragment>
    );
};
export default Home;