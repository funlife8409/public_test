import React from 'react';
type props = {
    value: string[],
    select: number[],
    name: string,
    id: string,
    onChange: any
}
const Home = ({ value = [], select=[], name = "", id = "", onChange }: props) => {
    let opt:any = (X:string, v:number) => {
        if (select.indexOf(v) > -1) {
            return <option key={v} defaultValue={v}>{X}</option>;
        }
        return <option key={v}>{X}</option>;
    };
    return (
        <select name={name} id={id} multiple multiselect-search="true" multiselect-select-all="true" multiselect-max-items="3" onChange={onChange}>
            {value.map(opt)}
        </select>
    );
};
export default Home;