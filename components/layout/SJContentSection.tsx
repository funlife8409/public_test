/**
 * @SJContentSection Single Job Content Section
 */

import React from 'react';
import { NextPage } from 'next'
import SJRCS from "./SJRightContentSection";

// typescript
import { Welcome } from '../../type'
interface props {
    data: Welcome;
    slug?: string;
}

type dataTitleName = {
    dates: {
        [key: string]: string
    },
    application_fee: {
        [key: string]: string
    }
}

const style = {
    one: {
        minHeight: "100px",
        backgroundColor: "blue",
    },
    get two() {
        return {
            minHeight: this.one.minHeight,
            backgroundColor: "red"
        }
    }
}
const Home: NextPage<props> = ({ data }) => {
    let Date_fee = () => {
        console.warn(data.dates, "-------")
        var dataTitleName: dataTitleName = {
            "dates": {
                "aplBegin": "Start Application",
                "lDate": "Last date of application",
                "lDatePayExFee": "Last Date For Pay Exam Fee",
                "exmDate": "Exam Date",
                "admCard": "Admit Card",
            },
            "application_fee": {
                "gen": "General",
                "obc": "OBC",
                "sc": "SC",
                "st": "ST",
                "phCand": "PH Candidates"
            }
        }
        let dateAr = [], feeAr = [];
        for (const key in data.dates) {
            if (Object.hasOwnProperty.call(data.dates, key)) {
                if (key === "other") {
                    data.dates.other.map((v, i) => {
                        if (v[0] || v[1]) {
                            console.log(v, i, "slkjdf")
                            // dateAr.push(i);
                            // data.dates[i] = v[0];
                            // dataTitleName.dates[i] = v[1];
                        }

                    });
                } else {
                    dateAr.push(key);
                }
            }
        }
        for (const key in data.application_fee) {
            if (Object.hasOwnProperty.call(data.application_fee, key)) {
                if (key === "other") {
                    data.application_fee.other.map((v, i) => {
                        console.log(v, "vv")
                        if (v[0] || v[1]) {
                            // feeAr.push(i);
                            // data.application_fee[i] = v[0];
                            // dataTitleName.application_fee[i] = v[1];
                        }

                    });
                } else {
                    feeAr.push(key);
                }
            }
        }

        let tex = "";
        if (dateAr.length > 0 && feeAr.length > 0) {
            let objLength = dateAr.length > feeAr.length ? dateAr.length : feeAr.length;
            for (let i = 0; i < objLength; i++) {
                tex += `<tr>${data.dates[dateAr[i]] ? `<td class="text-start">${dataTitleName.dates[dateAr[i]]}</td><td class="text-end border-end">${data.dates[dateAr[i]]}</td>` : `<td></td><td class="text-end border-end"></td>`}
            ${data.application_fee[feeAr[i]] ? `${dataTitleName.application_fee[feeAr[i]] ? `<td class="text-start">${dataTitleName.application_fee[feeAr[i]]}</td><td class="text-end">${data.application_fee[feeAr[i]]}</td>` : `<td colspan="2">${data.application_fee[feeAr[i]]}</td>`}` : "<td></td><td></td>"}</tr>`;
            }
        }
        return (
            <table className='datFee table table-borderless border-0 mt-5' border={1} cellPadding="1" cellSpacing="1" >
                <thead className='bg-primary text-white'>
                    <tr>
                        <td colSpan={2} rowSpan={1} className='py-2' style={{ width: "50%" }}>Important Dates</td>
                        <td colSpan={2} rowSpan={1} className='py-2'>Application Fee</td>
                    </tr>
                </thead>
                <tbody dangerouslySetInnerHTML={{ __html: tex }}>
                </tbody>
            </table>
        )
    }
    return (
        // (JDHS) Job Detail Header Section
        <div className="JDHS">
            <div className="container">
                <div className="row mt-4">
                    <div className="col-8 border p-3">
                        <h5>{data.basic_detail.secondry_name}</h5>
                        <p><small>Last Update {data.dates.lDate}</small></p>

                        {/* // Description */}
                        <p className='mt-4'>{data.sortDetail}</p>

                        {/* Dates and application fee */}
                        {/* <table className='datFee table table-borderless border-0 mt-5' border="1" cellPadding="1" cellSpacing="1" >
                            <thead className='bg-primary text-white'>
                                <tr>
                                    <td colSpan="2" rowSpan="1" className='py-2' style={{ width: "50%" }}>Important Dates</td>
                                    <td colSpan="2" rowSpan="1" className='py-2'>Application Fee</td>
                                </tr>
                            </thead>
                        </table> */}
                        <Date_fee />
                        <div dangerouslySetInnerHTML={{ __html: data.description }}></div>
                    </div>
                    <div className="col-4">
                        <SJRCS data={data} />
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Home;