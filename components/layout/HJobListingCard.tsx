import React from 'react';
import JobListing from '../layout/JobListing'

// import types
import { Welcome } from '../../type'

interface props {
    data: Welcome[];
    name: string;
}

const Home = ({ data, name }: props) => {
    var jobNoti = data.map((v, i) =>
        <div className="col-12" key={i}>
            <JobListing data={v} name={''} />
        </div>
    )

    return (
        <div className="card mt-5">
            <div className="card-header h5">
                {name}
                <i className="fas fa-expand float-end" style={{cursor: "pointer"}}></i>
            </div>
            <div className="card-body">
                {jobNoti}
            </div>

        </div>
    );
};

export default Home;