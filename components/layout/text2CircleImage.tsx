import { Inter } from '@next/font/google'
const inter = Inter({ subsets: ['latin'] })

export default function HomePageSearch({ value }: { value: string }) {
    return (
        <div className="chaLogo">
            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" xmlSpace="preserve" style={{ shapeRendering: "geometricPrecision", textRendering: "geometricPrecision", fillRule: "evenodd", clipRule: "evenodd" }}
                viewBox="0 0 500 500">
                <g id="UrTavla">
                    <circle cx="250" cy="250" r="245">
                    </circle>
                    <text x="50%" y="52%" textAnchor="middle" fontSize="13rem" stroke="rgb(196 136 27 / 100%)" strokeWidth="2px" dy=".3em">{value}</text>
                </g>
            </svg>
        </div>
    )
}