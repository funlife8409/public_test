import React from 'react';
import Image from 'next/image';
import { NextPage } from 'next'

// typescript
import { Links, Welcome } from '../../type'
interface props {
    data: Welcome;
    slug?: string;
}

interface linksObj {
    [key: string]: string;
    apyLink: string;
    downNotifLink: string;
    offWebLink: string;
    downAdmitLink: string;
    viewRes: string;
}

let style = {
    tags: {
        borderRadius: "35px",
        fontSize: "small"
    }
}
const Tags = ({ tags }: { tags: string[] }) => {
    const filteredTags = tags.filter(el => el);
    return (
        <p className='tags mt-3 overflow-hidden'>{filteredTags.map((v, i) => <span className='border me-1 px-3 py-1 font-weight-light' key={i} style={style.tags}>{v}</span>)}</p>
    )
}
const MiniInfo = ({ data }: props) => {
    return (
        <div className="row px-4" style={{ fontSize: "small" }}>
            {data.dates.aplBegin ? <div className="col text-center border-end mt-3"><b>20/12/2022</b><br />Begin</div> : <div></div>}
            {data.basic_detail.total_vacancy ? <div className="col text-center border-end mt-3"><b>{data.basic_detail.total_vacancy}</b><br />Posts</div> : <div></div>}
            {data.dates.lDate ? <div className="col text-center border-end mt-3"><b>{data.dates.lDate}</b><br />Posts</div> : <div></div>}
        </div>
    )
}
const Link = ({ links }: { links: Links }) => {
    // console.log(links)
    const linksObj:linksObj = {
        apyLink: "Apply Online",
        downNotifLink: "Download Notification",
        offWebLink: "Official Website",
        downAdmitLink: "Download Admit Card",
        viewRes: "View Result"
    }
    let nameAr: string[] = [];
    for (const key in links) {
        if (key !== "other") {
            if (links[key]) {
                nameAr.push(key);
            }
        }
    }

    return (
        <div className="row link mx-0 mt-4 small">
            {/* {links.apyLink ? <a href="" className='col-12 bg-primary text-white p-2 border-bottom text-center'>Apply</a> : ""} */}
            {nameAr.map((v, i) => <a href={links[v]} className='col-12 bg-primary text-white p-2 border-bottom text-center' key={i}>{linksObj[v]}</a>)}
            {links.other.map((v, i) => v[0] ? <a href={v[1]} className='col-12 bg-primary text-white p-2 border-bottom text-center' key={i}>{v[0]}</a> : "")}
        </div>
    )
}
const SJContentSection: NextPage<props> = ({ data }) => {
    return (
        <div className='border' style={{ height: '500px' }}>
            <span className='bg-success text-white badge font-weight-normal' style={{ padding: "7px 21px", position: 'relative', right: '7px', bottom: '3px' }}>Open</span>
            <div className="logo ms-auto p-3 rounded text-center pb-0" >
            <Image src={`/api/images/UY100_${data.basic_detail.logo}`} alt="" className="responsive" width={100} height={100} style={{ boxShadow: "0 1px 4px 0 rgb(0 0 0 / 25%), 0 1px 3px 0px rgb(0 0 0 / 5%) !important", borderRadius: "50%" }} />
                <h5 className='text-center mt-3'>{data.basic_detail.compny_name}</h5>
                <p className="mb-1 font-weight-light">{data.basic_detail.compny_type}</p>
                <Tags tags={[...data.tags.tags, ...data.tags.other]} />
            </div>
            <MiniInfo data={data} />
            <Link links={data.links} />
            {/* <div className="row link mx-0 mt-4 small">
                <a href="" className='col-12 bg-primary text-white p-2 border-bottom text-center'>Apply</a>
                <a href="" className='col-12 bg-primary text-white p-2 border-bottom text-center'>Download Notification</a>
                <a href="" className='col-12 bg-primary text-white p-2 border-bottom text-center'>Official Website</a>
            </div> */}
        </div>
    )
}
export default SJContentSection;