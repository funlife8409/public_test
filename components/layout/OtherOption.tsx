import React, { useState } from 'react';
import Input from './Input'

type props = {
    name?: string,
    id?: string,
    onChange?: any,
    placeholderName?: string[],
}

const Home = ({ name = "", id = "", onChange, placeholderName = ["Other Title", "Other Value"] }: props) => {
    const [socName, setSocName] = useState<string[]>([]);
    const moreOption = (e:string) => {
        setSocName([...socName, e + "_" + (socName.length + 1)]);
    }
    let appendOpt = function (X:string, v:number) {
        return <div className="col-6 mt-4" key={v}>
            <div className="row">
                <div className="col pe-0">
                    <Input id={X} name={X} title={placeholderName[0] + " - " + (v + 1)} />
                </div>
                <div className="col ps-1">
                    <Input id={X + "_val"} name={X + "_val"} title={placeholderName[1] + " - " + (v + 1)} />
                </div>
            </div>
        </div>;
    };
    return (
        <React.Fragment>
            <div className="col-6 mt-4">
                <div className="row">
                    <div className="col pe-0">
                        <Input id={name} name={name} title={placeholderName[0]} />
                    </div>
                    <div className="col ps-1">
                        <Input id={name + "_val"} name={name + "_val"} title={placeholderName[1]} />
                    </div>
                    <div className="col-3 ps-0">
                        <button type="button" className="btn btn-primary" onClick={() => moreOption(name)}>+</button>
                    </div>
                </div>
            </div>
            {socName.map(appendOpt)}
        </React.Fragment>
    );
};
export default Home;