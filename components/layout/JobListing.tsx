import React from 'react';
import Image from 'next/image';
import T2I from './text2CircleImage';
import Link from "next/link";
// import { Link } from 'react-router-dom';

// import types
import { Welcome } from '../../type';

interface props {
    data: Welcome;
    name: string;
}

const Home = ({data}:props) => {
        const leftDate = () => {
            var obj:{name: string,value:string} = Object(),
            type = data.type;
            // obj = {
            //     name: String,
            //     value: String
            // }
            if (data.publishDate)
                obj.name = "Post Update:- ",
                    obj.value = data.publishDate.toString()
            return obj;
        };
        const rightDate = () => {
            var obj:{name: string,value:string} = Object(),
            type = data.type;
            // obj = {
            //     name: String,
            //     value: String
            // }
            if (data.type) {
                if (type == "admitcard") {
                    obj.name = "Result Declared:- ",
                        obj.value = data.dates.exmDate
                } else if (type == "latest") {
                    obj.name = "Last Date:- ",
                        obj.value = data.dates.lDate
                } else {
                    obj.name = "",
                        obj.value = ""
                }
                return obj;
            }
            else {
                return {
                    name: "",
                    value: ""
                }
            }
        };
        const logo = () => {
            if (data.logo)
                return <Image src={data.logo} alt={''} width={40} height={40}/>
            else
                console.log("Chek vivekkkkkkkkkk", data.title)
                return <T2I value={data.title[0].toUpperCase()} />
        }
        const url = () => {
            return "/job/" + data.permalink;
        }
        return (
            <div>
                <div className="card shadow-0">
                    <div className="card-body row jobListingCard d-flex align-items-center justify-content-center">
                        <div className="col-2 cardLogo text-center"><Link href={url()}>{logo()}</Link></div>
                        <div className="col cardContent">
                            <h5 className="card-title"><Link href={url()}>{data.title}</Link></h5>
                            <p className="m-0"> <b>{leftDate().name} </b> {leftDate().value}&nbsp; <b> {rightDate().name} </b>{rightDate().value}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
};
export default Home;