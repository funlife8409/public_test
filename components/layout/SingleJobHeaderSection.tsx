import React from 'react';
import Image from 'next/image';
import { Welcome } from "../../type";

interface props {
    data: Welcome;
    slug?: string;
}

const style = {
    one: {
        minHeight: "250px"
    },
    get two() {
        return {
            minHeight: this.one.minHeight
        }
    },
    logo: {
        width: "140px",
        boxShadow: "0 1px 4px 0 rgba(0, 0, 0, .25), 0 1px 3px 0px rgba(0, 0, 0, 0.05) !important"
    },
    tableTd: {
        width: "30%"
    }

}
let tDate = () => {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); // January is 0!
    var yyyy = today.getFullYear();

    return dd + '-' + mm + '-' + yyyy;
}
let age = ({ minAge, maxAge }: { minAge: string, maxAge: string }) => {
    if (maxAge && minAge) {
        return `${minAge} - ${maxAge} Years`;
    } else if (!maxAge && minAge) {
        return `+${minAge} Years`;
    } else if (!minAge && maxAge) {
        return `-${maxAge} Years`;
    } else {
        return null;
    }
}
const Home = ({ data }:props) => {
    var button = (
        <span className="btn-primary ms-2 rounded-3" style={{ padding: "5px 10px", paddingRight: "0", backgroundColor: "#1266f1bf" }}>
            <i className="fa-solid fa-check"></i>
            <span className="bg-primary rounded-3" style={{ padding: "5px 13px", marginLeft: "8px" }}>
                Verified
            </span>
        </span>
    )
    return (
        // (JDHS) Job Detail Header Section
        <div className="JDHS">
            <div className="container-fluid">
                <div className="row shadow-4 border-primary" style={{ borderBottom: "2px solid", boxShadow: "0 6px 30px 0 rgba(0,0,0,.05)!important" }}>
                    <div className="col-md-8 d-flex align-items-center justify-content-center" style={style.one}>
                        <div className="container">
                            <div className="row">
                                <div className="col-sm-4">
                                    <div className="logo ms-auto p-3 rounded" style={style.logo}>
                                        {/* <img src={`http://localhost:3000/api/images/UY100_${data.basic_detail.logo}`} className="" alt="" style={{ width: "100%" }} /> */}
                                        <Image src="https://marketplace.canva.com/EAFaFUz4aKo/2/0/1600w/canva-yellow-abstract-cooking-fire-free-logo-JmYWTjUsE-Q.jpg" alt="" className="responsive" width={100} height={100}/>
                                    </div>
                                </div>
                                <div className="col ms-5 d-flex border-end">
                                    <div className="my-auto">
                                        <h4>{data.title}</h4>
                                        <p className="display-6" style={{ fontSize: "1.3rem" }}>{data.basic_detail.Orgnization}</p>
                                        <p style={{ fontSize: "0.9rem" }}><i className="fas fa-camera-retro fa-md" /> {data.basic_detail.total_vacancy} Vacancy &nbsp;&nbsp;<i className="fas fa-briefcase fa-md" /> {data.type[0].toUpperCase() + data.type.slice(1)} &nbsp;&nbsp;<i className="flag flag-india"></i> India &nbsp;&nbsp;{button}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4 d-flex align-items-center justify-content-center" style={style.two}>
                        <table className="table table-borderless">
                            <tbody>
                                <tr>
                                    <th className="fw-normal py-2" style={style.tableTd}>Today Date</th>
                                    <td className="py-2">{tDate()}</td>
                                </tr>
                                <tr>
                                    <th className="fw-normal py-2" style={style.tableTd}>Age</th>
                                    <td className="py-2">{age({ minAge: data.basic_detail.minAge, maxAge: data.basic_detail.maxAge })}</td>
                                </tr>
                                <tr>
                                    <th className="fw-normal py-2" style={style.tableTd}>Share</th>
                                    <td className="py-2"><a href="">Click here</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    );
};
export default Home;